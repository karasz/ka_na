# KA_NA Project
Stuff made for KA_NA project in GO
Vendor is not checked in the repository, in order to build one needs to:

```shell
    mkdir -p work
    cd work
    git clone https://karasz@bitbucket.org/karasz/ka_na.git
    cd ka_na
    make setup
    make
    ./bin/ka_na
```
Happy hunting!
