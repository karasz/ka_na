package main

import (
	"database/sql"
	"fmt"

	_ "github.com/go-sql-driver/mysql"
)

// UserData is a struct containing a person and its data
type UserData struct {
	ID        string `json:"id"`
	Firstname string `json:"firstname"`
	Lastname  string `json:"lastname"`
	Address   string `json:"address"`
	Email     string `json:"email"`
	Tel       string `json:"tel"`
}

func dbConn() (db *sql.DB) {
	dbDriver := "mysql"
	dbUser := "ka_na"
	dbPass := "password"
	dbName := "ka_na"
	db, err := sql.Open(dbDriver, dbUser+":"+dbPass+"@tcp(127.0.0.1:3306)/"+dbName)
	if err != nil {
		panic(err.Error())
	}
	fmt.Println(db)
	return db
}

func getUserFromDB(e string) *UserData {
	db := dbConn()

	defer db.Close()
	var u UserData
	err := db.QueryRow("SELECT id, firstname, lastname, address, email, tel FROM users WHERE email=?", e).Scan(&u.ID, &u.Firstname, &u.Lastname, &u.Address, &u.Email, &u.Tel)
	if err != nil {
		fmt.Println(err)
		return nil
	}
	return &u

}

func addUserToDB(u *UserData) error {
	db := dbConn()
	defer db.Close()

	insert, err := db.Prepare("INSERT INTO users(firstname, lastname, address, email, tel) VALUES ( ?,?,?,?,?)")
	insert.Exec(u.Firstname, u.Lastname, u.Address, u.Email, u.Tel)
	return err
}

func deleteUserFromDB(s string) error {
	db := dbConn()
	defer db.Close()
	del, err := db.Prepare("DELETE FROM users WHERE email=?")
	del.Exec(s)
	return err
}

func updateUserInDB(u *UserData, e string) error {
	db := dbConn()
	defer db.Close()

	v := getUserFromDB(e)
	v.Email = u.Email
	v.Firstname = u.Firstname
	v.Lastname = u.Lastname
	v.Address = u.Address
	v.Tel = u.Tel
	update, err := db.Prepare("UPDATE users SET email=?,firstname=?, lastname=?, address=?, tel=? WHERE users.email=?")
	update.Exec(v.Email, v.Firstname, v.Lastname, v.Address, v.Tel, e)
	return err
}
