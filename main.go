package main

import (
	"encoding/gob"
	"fmt"
	"html/template"
	"io/ioutil"
	"log"
	"net/http"
	"net/url"

	"github.com/ConradIrwin/xsrftoken"
	"github.com/google/identity-toolkit-go-client/gitkit"
	"github.com/gorilla/mux"
	"github.com/gorilla/sessions"
	"golang.org/x/net/context"
	gomail "gopkg.in/gomail.v2"
)

// Action URLs.
const (
	indexURL         = "/"
	widgetURL        = "/user"
	signOutURL       = "/signOut"
	oobActionURL     = "/oobAction"
	updateURL        = "/update"
	deleteAccountURL = "/deleteAccount"
)
const (
	indexTemmplatePath = "tpls/index.tmpl"
	userTemplatePath   = "tpls/user.tmpl"
)

var (
	indexTemplate = template.Must(template.ParseFiles(indexTemmplatePath))
	userTemplate  = template.Must(template.ParseFiles(userTemplatePath))
)

var (
	client      *gitkit.Client
	cookieStore *sessions.CookieStore
	xsrfKey     string
)

// Identity toolkit configurations.
const (
	browserAPIKey = "AIzaSyBnFWb3CffDeUxhZLtqN_OXjKCro0DwU2A"
	clientID      = "33933773656-0st9hmo5oaoeo375im32mns5n6kanv4he.apps.googleusercontent.com"
	projID        = "ka-na-22"
)

var tmpl = template.Must(template.ParseGlob("tpls/*"))

const (
	gtokenCookieName = "gtoken"
	sessionName      = "SESSIONID"
	xsrfTokenName    = "xsrftoken"
)

// Email templates.
const (
	emailTemplateResetPassword = `<p>Dear user,</p>
<p>
Forgot your password?<br>
KA_NA received a request to reset the password for your account <b>%[1]s</b>.<br>
To reset your password, click on the link below (or copy and paste the URL into your browser):<br>
<a href="%[2]s">%[2]s</a><br>
</p>
<p>KA_NA Support</p>`

	emailTemplateChangeEmail = `<p>Dear user,</p>
<p>
Want to use another email address to sign into KA_NA?<br>
KA_NA received a request to change your account email address from %[1]s to <b>%[2]s</b>.<br>
To change your account email address, click on the link below (or copy and paste the URL into your browser):<br>
<a href="%[3]s">%[3]s</a><br>
</p>
<p>KA_NA Support</p>`

	emailTemplateVerifyEmail = `Dear user,
<p>Thank you for creating an account on KA_NA.</p>
<p>To verify your account email address, click on the link below (or copy and paste the URL into your browser):</p>
<p><a href="%[1]s">%[1]s</a></p>
<br>
<p>KA_NA Support</p>`
)

// SessionUserKey type
type SessionUserKey int

// Key used to store the user information in the current session.
const sessionUserKey SessionUserKey = 0

// saveCurrentUser stores the user information in current session.
func saveCurrentUser(r *http.Request, w http.ResponseWriter, u *gitkit.User) {
	if u == nil {
		return
	}
	s, _ := cookieStore.Get(r, sessionName)
	s.Values[sessionUserKey] = *u
	err := s.Save(r, w)
	if err != nil {
		fmt.Println(fmt.Errorf("Cannot save session: %s", err))
	}
	uu := UserData{}
	uu.Email = u.Email
	err = addUserToDB(&uu)
	if err != nil {
		fmt.Println(fmt.Errorf("Cannot save email: %s", err))
	}
}

func handleIndex(w http.ResponseWriter, r *http.Request) {
	u := validateToken(r)
	saveCurrentUser(r, w, u)
	var xf, xd string
	var usr *UserData
	if u != nil {
		xf = xsrftoken.Generate(xsrfKey, u.LocalID, updateURL)
		xd = xsrftoken.Generate(xsrfKey, u.LocalID, deleteAccountURL)
		usr = getUserFromDB(u.Email)
		err := indexTemplate.Execute(
			w,
			struct {
				WidgetURL              string
				SignOutURL             string
				User                   *gitkit.User
				Firstname              string
				Lastname               string
				Email                  string
				Address                string
				Tel                    string
				UpdateUserURL          string
				UpdateUserXSRFToken    string
				DeleteAccountURL       string
				DeleteAccountXSRFToken string
			}{widgetURL, signOutURL, u, usr.Firstname, usr.Lastname, usr.Email, usr.Address, usr.Tel, updateURL, xf, deleteAccountURL, xd})
		if err != nil {
			fmt.Println(err)
		}
	} else {
		err := indexTemplate.Execute(
			w,
			struct {
				WidgetURL string
				User      *gitkit.User
			}{widgetURL, nil})
		if err != nil {
			fmt.Println(err)
		}
	}
}

func handleWidget(w http.ResponseWriter, r *http.Request) {
	defer r.Body.Close()
	// Extract the POST body if any.
	b, _ := ioutil.ReadAll(r.Body)
	body, _ := url.QueryUnescape(string(b))
	userTemplate.Execute(
		w,
		struct {
			BrowserAPIKey    string
			SignInSuccessURL string
			OOBActionURL     string
			POSTBody         string
		}{browserAPIKey, indexURL, oobActionURL, body})
}

func handleSignOut(w http.ResponseWriter, r *http.Request) {
	s, _ := cookieStore.Get(r, sessionName)
	s.Options = &sessions.Options{
		MaxAge: -1, // MaxAge<0 means delete session cookie.
	}
	err := s.Save(r, w)
	if err != nil {
		fmt.Println(fmt.Errorf("Cannot save session: %s", err))
	}
	// Also clear identity toolkit token.
	http.SetCookie(w, &http.Cookie{Name: gtokenCookieName, MaxAge: -1})
	// Redirect to home page for sign in again.
	http.Redirect(w, r, indexURL, http.StatusFound)
}

func handleOOBAction(w http.ResponseWriter, r *http.Request) {
	c := r.Context()
	resp, err := client.GenerateOOBCode(c, r)
	if err != nil {
		fmt.Println("Failed to get an OOB code:", err)
		w.Write([]byte(gitkit.ErrorResponse(err)))
		return
	}
	msg := gomail.NewMessage()
	msg.SetHeader("From", "<nagy.karolygabriel@gmail.com>")

	switch resp.Action {
	case gitkit.OOBActionResetPassword:
		msg.SetHeader("Subject", "Reset your KA_NA account password")
		msg.SetBody("text/html", fmt.Sprintf(emailTemplateResetPassword, resp.Email, resp.OOBCodeURL.String()))
		msg.SetHeader("To", []string{resp.Email}[0])
	case gitkit.OOBActionChangeEmail:
		msg.SetHeader("Subject", "KA_NA account email address change confirmation")
		msg.SetBody("text/html", fmt.Sprintf(emailTemplateChangeEmail, resp.Email, resp.NewEmail, resp.OOBCodeURL.String()))
		msg.SetHeader("To", []string{resp.NewEmail}[0])
	case gitkit.OOBActionVerifyEmail:
		msg.SetHeader("Subject", "KA_NA account registration confirmation")
		msg.SetBody("text/html", fmt.Sprintf(emailTemplateVerifyEmail, resp.OOBCodeURL.String()))
		msg.SetHeader("To", []string{resp.Email}[0])
	}
	if err := submitMail(msg); err != nil {
		fmt.Println("Failed to send message to user", resp.Action, resp.Email, err)
		w.Write([]byte(gitkit.ErrorResponse(err)))
		return
	}
	w.Write([]byte(gitkit.SuccessResponse()))
}

func handleUpdate(w http.ResponseWriter, r *http.Request) {

	// Check if there is a signed in user.
	u := validateToken(r)
	uuu := UserData{}
	var err error
	if u == nil {
		fmt.Println("No signed in user for updating")
		goto out
	}
	// Validate XSRF token first.
	if !xsrftoken.Valid(r.PostFormValue(xsrfTokenName), xsrfKey, u.LocalID, updateURL) {
		fmt.Println("XSRF token validation failed")
		goto out
	}
	// Extract the new data.
	// we create an UserData and fill it from r.PostFromValue
	uuu.Firstname = r.PostFormValue("FirstName")
	uuu.Email = r.PostFormValue("Email")
	uuu.Lastname = r.PostFormValue("LastName")
	uuu.Address = r.PostFormValue("Address")
	uuu.Tel = r.PostFormValue("Telephone")
	err = updateUserInDB(&uuu, u.Email)
	if err != nil {
		fmt.Println(err)
		goto out
	}
out:
	// Redirect to home page to show the update result.
	http.Redirect(w, r, indexURL, http.StatusFound)
}

func handleDeleteAccount(w http.ResponseWriter, r *http.Request) {
	c := context.Background()

	// Check if there is a signed in user.
	u := validateToken(r)
	if u == nil {
		fmt.Println("No signed in user for updating")
		goto out
	}
	// Validate XSRF token first.
	if !xsrftoken.Valid(r.PostFormValue(xsrfTokenName), xsrfKey, u.LocalID, deleteAccountURL) {
		fmt.Println("XSRF token validation failed")
		goto out
	}
	_ = deleteUserFromDB(u.Email)
	// Delete account.
	if err := client.DeleteUser(c, &gitkit.User{LocalID: u.LocalID}); err != nil {
		fmt.Printf("Failed to delete user %+v: %s/n", *u, err)
		goto out
	}

	// Account deletion succeeded. Call sign out to clear session and identity
	// toolkit token.
	handleSignOut(w, r)
	return
out:
	http.Redirect(w, r, indexURL, http.StatusFound)
}

func initClient() {
	config := &gitkit.Config{
		WidgetURL:  "https://ka-na.karasz.im",
		CookieName: gtokenCookieName,
	}
	var err error

	if client, err = gitkit.New(context.Background(), config); err != nil {
		fmt.Println(err)
	}
}

func validateToken(r *http.Request) *gitkit.User {
	ctx := context.Background()
	s, _ := cookieStore.Get(r, sessionName)
	if s.IsNew {
		ts := client.TokenFromRequest(r)
		if ts == "" {
			return nil
		}
		token, err := client.ValidateToken(ctx, ts, []string{projID})
		if err != nil {
			fmt.Println(err)
			return nil
		}
		u, err := client.UserByEmail(ctx, token.Email)
		if err != nil {
			fmt.Println("Failed to fetch user info")
			return nil
		}
		return u
	}
	// Extracts user from current session.
	v, ok := s.Values[sessionUserKey]
	if !ok {
		fmt.Println("no user found in current session")
	}
	return v.(*gitkit.User)
}

func main() {
	// Register datatypes for saving in session
	gob.Register(SessionUserKey(0))
	gob.Register(&gitkit.User{})

	// Initialize XSRF token key.
	xsrfKey = "My very secure XSRF token key"

	cookieStore = sessions.NewCookieStore(
		[]byte("My very secure authentication key for cookie store or generate one using securecookies.GenerateRamdonKey()")[:64],
		[]byte("My very secure encryption key for cookie store or generate one using securecookies.GenerateRamdonKey()")[:32])
	cookieStore.Options = &sessions.Options{
		MaxAge:   86400 * 7, // Session valid for one week.
		HttpOnly: true,
	}
	initClient()

	myRouter := mux.NewRouter().StrictSlash(true)
	myRouter.HandleFunc(indexURL, handleIndex)
	myRouter.HandleFunc(widgetURL, handleWidget)
	myRouter.HandleFunc(signOutURL, handleSignOut)
	myRouter.HandleFunc(oobActionURL, handleOOBAction)
	myRouter.HandleFunc(updateURL, handleUpdate)
	myRouter.HandleFunc(deleteAccountURL, handleDeleteAccount)

	log.Fatal(http.ListenAndServe("127.0.0.1:8080", myRouter))

}
